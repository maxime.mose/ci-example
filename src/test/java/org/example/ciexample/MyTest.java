package org.example.ciexample;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;

public class MyTest {

    private final RequestSpecification specification = RestAssured.given()
            .baseUri("https://petstore.swagger.io")
            .basePath("v2/pet")
            .param("status", "available")
            .log().all();

    @Test
    void test() {
        Response postRs = specification.get("findByStatus").prettyPeek();
        assertFalse(postRs.jsonPath().getList("").isEmpty());
    }
}
